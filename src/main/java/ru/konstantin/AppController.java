package ru.konstantin;

import com.google.gson.*;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.HtmlUtils;
import ru.konstantin.crypto.AES;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
@Scope(value = "prototype")
public class AppController {
    private static final String MY_SECRET_WORD = "Itisagooddaytofun";//секретное слово для шифрования и дешифровки
    private static final String ACCESS_TOKEN = "e18850ac95aadd0d226eb52301e86a5c9919873253acba9e629267d8a31579376283796291587f4405dff";//Ключ доступа VK World

    @RequestMapping(value = "/")
    public String startHere() {
        return "index";
    }

    //Секретные ключи
    //aae1c99dc7c4d54c5472b894f5d3678df9b61b89fb17fd37dd7f9b64ec09d4fe703312bf863f50d220ae4 - малая группа
    //2780180993a398217e4e5cbdaeae97c2cdbe0a2edb7561b509221743c4df8c95e8cd5b30857da19d4c444 - большая группа

//	@RequestMapping(value = "/vk-answer", method = RequestMethod.GET)
//	public String vkAnswer(String s) {
//		System.out.println(s);
//		return "index";
//	}

    //Шифруем строку, которую должен вернуть сервер
    @RequestMapping(value = "/{vk-request}", method = RequestMethod.GET)
    public String encryptAnswer(@PathVariable("vk-request") String answerForConfirmation, Model model) {
        String encryptString = new AES().encrypt(answerForConfirmation, MY_SECRET_WORD);
        model.addAttribute("code", encryptString);
        return "standart-answer";
    }

    @RequestMapping(value = "/{vk-request}", method = RequestMethod.POST)
    public String vkAnswer(@PathVariable("vk-request") String answerForConfirmation, HttpServletRequest httpServletRequest, Model model) {
        String userForGetMessages = "229957902";
        String body = "-1";
        try {
            body = httpServletRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            //Read the JSON file
            JsonElement root = new JsonParser().parse(body);
            String vkMessageType = root.getAsJsonObject().get("type").getAsString();

            String decryptString = new AES().decrypt(answerForConfirmation, MY_SECRET_WORD);
            System.out.println(decryptString + "---" + vkMessageType + "\n" + body);

            switch (vkMessageType) {
                case "message_new":

                    break;
                case "wall_reply_new":
                    CloseableHttpClient httpClient = null;
                    try {
                        httpClient = HttpClients.createDefault();

                        JsonObject object = root.getAsJsonObject().get("object").getAsJsonObject();
                        String userId = object.get("from_id").toString();//получаем id пользователя, который оставил комментарий

                        //Получаем имя пользователя
                        HttpGet getUserName = new HttpGet("https://api.vk.com/method/users.get?user_ids=" + userId + "&v=5.0");
                        String name = EntityUtils.toString(httpClient.execute(getUserName).getEntity());

                        String userName = null;
                        JsonElement comentText = null;//получаем id пользователя, который оставил комментарий
                        StringBuilder httpQuery = new StringBuilder();//%7C
                        comentText = object.get("text");

                        try {
                            userName = new JsonParser().parse(name).getAsJsonObject().get("response").getAsJsonArray().get(0).getAsJsonObject().get("first_name").toString().replaceAll("\"", "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        userId = "id" + userId;
                        httpQuery.append("https://api.vk.com/method/messages.send?")
                                .append("message=");
                        if (userId.contains("-")) {
                            httpQuery.append(URLEncoder.encode(" КСД коментирует:\n ", "UTF-8"));

                        } else {
                            httpQuery.append(URLEncoder.encode("[" + userId + "|" + userName + "] коментирует:\n ", "UTF-8"));
                        }

                        httpQuery.append(URLEncoder.encode(comentText.toString().replaceAll("\"", ""), "UTF-8"))//URLEncoder.encode("Мария", "UTF-8")
                                .append("&user_id=").append(userForGetMessages)
                                .append("&access_token=").append(ACCESS_TOKEN)
                                .append("&v=5.50");
                        //create GET request

//                    System.out.println(HtmlUtils.htmlEscape(httpQuery.toString()));
//                    String httpQueryEscape = URLEncoder.encode(httpQuery.toString(), "UTF-8");
//                    System.out.println(httpQueryEscape);
                        HttpGet httpGet = new HttpGet(httpQuery.toString());
                        httpClient.execute(httpGet);
                    } catch (IOException | ParseException e) {
                        e.printStackTrace();
                    } finally {
                        assert httpClient != null;
                        httpClient.close();
                    }
                    break;
            }

            if (vkMessageType.equalsIgnoreCase("confirmation")) {
                model.addAttribute("code", decryptString);
            } else {
                model.addAttribute("code", "ok");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "standart-answer";
    }
}